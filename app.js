document.addEventListener('DOMContentLoaded', function() {
//     if (window.location.pathname !== '/') {
//         return false;
//     }

    if (!('Notification' in window)) {
        throw new Error('Browser is not supported Push-notifications');
    }

    let push = new Push({
        senderId: '26399460559',
        debug: true,
        url: 'static-pages/confirm-tech'
    });

    try {
        push.subscribe();
    } catch (exception) {
        console.warn(exception.message);
    }

});

/**
 * Отправка Push-уведомлений
 */
class Push {
constructor(options) {

        this.url = options.url || null;
        this.debug = options.debug || false;
        this.senderId = options.senderId || null;

        let self = this;

        navigator.serviceWorker.register('./firebase-messaging-sw.js').then(function(registration) {
            if (self.debug) {
                console.log('Registration success with scope: ', registration.scope);
            }

            

        }).catch(function(error) {
            if (self.debug) {
                console.log('Registration failed: ', error);
            }

            throw new Error('Error: ' + error.message);
        });
    
           firebase.initializeApp({
                apiKey: "AIzaSyAliMk5tItIPULbuTQ5FdwTaHFIV8HZ7t4",
                authDomain: "amplified-album-190310.firebaseapp.com",
                databaseURL: "https://amplified-album-190310.firebaseio.com",
                projectId: "amplified-album-190310",
                storageBucket: "amplified-album-190310.appspot.com",
                messagingSenderId: "778050686361"
            });
        

        this.messaging = firebase.messaging();

        /**
         * Проверка подписки на уведомления
         * @param token
         * @returns {boolean}
         */
        this.isToken = function(token) {
            return window.localStorage.getItem('firebaseMessagingToken') === token;
        };

        /**
         * Подписка на уведомления
         * @param token
         */
        this.setToken = function(token) {
            window.localStorage.setItem('firebaseMessagingToken',
                token ? token : ''
            );
        };

        /**
         * Отправка токена на сервер
         * @param url
         * @param token
         * @returns {boolean}
         */
        this.sendToken = function(url, token) {
            let self = this;

            if (this.debug) {
                console.log(token);
            }

            if (this.isToken(token)) {
                return false;
            }

            fetch(url, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: 'POST',
                body: JSON.stringify({
                    currentToken: token
                })
            })
                .then(function() {
                    self.setToken(token);
                });
        }
    }


    /**
     * Подписка на уведомления
     */
    subscribe() {
        let self = this;

        this.messaging.requestPermission()
            .then(function() {
                self.messaging.getToken()
                    .then(function(currentToken) {

                        if (self.debug) {
                            console.log(currentToken);
                        }

                        if (!currentToken) {
                            self.setToken(false);

                            throw new Error('Could not get token');
                        }

                        self.sendToken(self.url, currentToken);

                    })
                    .catch(function(error) {
                        self.setToken(false);

                        throw new Error('Error: ' + error);
                    });
            })
            .catch(function(error) {
                throw new Error('Error: ' + error.message);
            });
    }
}
